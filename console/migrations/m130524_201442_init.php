<?php

use yii\db\Migration;

class m130524_201442_init extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'staff_no' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
                ], $tableOptions);

        $this->createTable('{{%profile}}', [
            'id' => $this->primaryKey(),
            'staff_no' => $this->string()->notNull()->unique(),
            'name' => $this->string(),
            'department' => $this->string(),
            'position' => $this->string(),
                ], $tableOptions);

        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'admin1',
            'staff_no' => '10272',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('admin1'),
            'email' => 'admin1@gmail.com',
            'status' => 10,
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('{{%profile}}', [
            'id' => 1,
            'staff_no' => '10272',
            'name' => 'KAMAL ADLI BIN BAKI',
            'department' => 'JABATAN MEKANIKAL',
            'position' => 'JURUGEGAS',
        ]);


        $this->createTable('{{%item}}', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer(),
            'geolocation_id' => $this->integer(),
            'category_id' => $this->integer(),
            'type' => $this->string(),
            'brand' => $this->string(),
            'model' => $this->string(),
            'horse_power' => $this->string(),
            'serial_no' => $this->string(),
            'kew_pa' => $this->string(),
            'floor' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'is_deleted' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
                ], $tableOptions);

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'is_deleted' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
                ], $tableOptions);

        $this->createTable('{{%site}}', [
            'id' => $this->primaryKey(),
            'state' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'is_deleted' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
                ], $tableOptions);

        $this->createTable('{{%geolocation}}', [
            'id' => $this->primaryKey(),
            'short_code' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_by' => $this->timestamp(),
            'is_deleted' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
                ], $tableOptions);

        $this->addForeignKey('fk-user-profile', '{{%profile}}', 'staff_no', '{{%user}}', 'staff_no');
        $this->addForeignKey('fk-item-category_id', '{{%item}}', 'category_id', '{{%category}}', 'id');
        $this->addForeignKey('fk-item-geolocation_id', '{{%item}}', 'geolocation_id', '{{%geolocation}}', 'id');
        $this->addForeignKey('fk-item-site_id', '{{%item}}', 'site_id', '{{%site}}', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk-user-profile', '{{%profile}}');
        $this->dropForeignKey('fk-item-category_id', '{{%item}}');
        $this->dropForeignKey('fk-item-geolocation_id', '{{%item}}');
        $this->dropForeignKey('fk-item-site_id', '{{%item}}');
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%profile}}');
        $this->dropTable('{{%item}}');
        $this->dropTable('{{%category}}');
        $this->dropTable('{{%site}}');
        $this->dropTable('{{%geolocation}}');
    }

}
