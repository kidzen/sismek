<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = $model-><?= $generator->getNameAttribute() ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info <?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i><?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?> : <?= "<?= " ?> Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
        <p>
            <?= "<?= " ?>Html::a(<?= $generator->generateString('Back') ?>, ['index'], ['class' => 'btn btn-default']) ?>
            <?= "<?= " ?>Html::a(<?= $generator->generateString('Update') ?>, ['update', <?= $urlParams ?>], ['class' => 'btn btn-primary']) ?>
            <?= "<?= " ?>Html::a(<?= $generator->generateString('Delete') ?>, ['delete', <?= $urlParams ?>], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => <?= $generator->generateString('Are you sure you want to delete this item?') ?>,
            'method' => 'post',
            ],
            ]) ?>
        </p>
        <?= "<?= " ?>DetailView::widget([
        'model' => $model,
        'attributes' => [
        <?php
        if (($tableSchema = $generator->getTableSchema()) === false) {
            foreach ($generator->getColumnNames() as $name) {
                echo "            '" . $name . "',\n";
            }
        } else {
            foreach ($generator->getTableSchema()->columns as $column) {
                $format = $generator->generateColumnFormat($column);
                if ($column->name == 'DELETED') {
                    echo "            [\n";
                    echo "            'attribute'=>'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                    echo "            'vAlign' => 'middle',\n";
                    echo "            'value' => \$model->DELETED == 1 ? 'Deleted' : 'Active',\n";
                    echo "            ],\n";
                } else {
                    echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                }
            }
        }
        ?>
        ],
        ]) ?>

    </div>
