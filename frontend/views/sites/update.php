<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sites */
?>
<div class="sites-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
