<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Geolocation;

/* @var $this yii\web\View */
/* @var $model common\models\Sites */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sites-form">

    <?php $form = ActiveForm::begin(); ?>


    <?=
            $form->field($model, 'geolocation_id')->dropDownList(ArrayHelper::map(Geolocation::find()->asArray()->all(), 'id', 'description'))
            ->label(Html::tag('span', 'Type')
                    . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/category-type/create']
                                    , ['role' => 'modal-remote', 'title' => 'Create new Categories', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
            )
    ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
