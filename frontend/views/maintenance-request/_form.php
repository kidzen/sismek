<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use common\models\Transaction;

/* @var $this yii\web\View */
/* @var $model common\models\MaintenanceRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maintenance-request-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'request_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'expected_date')->textInput(['maxlength' => true]) ?>



    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Send' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
