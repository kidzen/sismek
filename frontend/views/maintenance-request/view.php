<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MaintenanceRequest */
?>
<div class="maintenance-request-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'transaction_id',
            'type',
            'request_date',
            'expected_date',
            'status',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'deleted',
            'deleted_at',
        ],
    ]) ?>

</div>
