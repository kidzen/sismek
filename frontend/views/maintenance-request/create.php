<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MaintenanceRequest */

?>
<div class="maintenance-request-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
