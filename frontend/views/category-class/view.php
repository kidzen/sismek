<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryClass */
?>
<div class="category-class-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description',
            'status',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'deleted',
            'deleted_at',
        ],
    ]) ?>

</div>
