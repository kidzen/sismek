<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryClass */
?>
<div class="category-class-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
