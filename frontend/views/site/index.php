<?php
/* @var $this yii\web\View */

$this->title = 'Dashboard (example)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-upload"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Permohonan Peneyelenggaraan</span>
                    <span class="info-box-number">0<!-- <small>%</small>--></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-gears"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Dalam Proses Selenggara</span>
                    <span class="info-box-number">0</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-clock-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Menghampiri Tarikh Servis</span>
                    <span class="info-box-number">0</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-check"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Siap Servis</span>
                    <span class="info-box-number">0</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable ui-sortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="nav-tabs-custom" style="cursor: move;">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right ui-sortable-handle">
                    <li class="active"><a href="#revenue-chart" data-toggle="tab">Graf</a></li>
                    <li><a href="#sales-chart" data-toggle="tab">Carta Pie</a></li>
                    <li class="pull-left header"><i class="fa fa-inbox"></i> Analisis Bulanan</li>
                </ul>
                <div class="tab-content no-padding">
                    <!-- Morris chart - Sales -->
                    <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="300" version="1.1" width="546" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative; left: -0.571442px; top: -0.714294px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="49.203125" y="261.859375" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4.2734375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#aaaaaa" d="M61.703125,261.859375H521" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="49.203125" y="202.64453125" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4.27734375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">7,500</tspan></text><path fill="none" stroke="#aaaaaa" d="M61.703125,202.64453125H521" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="49.203125" y="143.4296875" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4.28125" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">15,000</tspan></text><path fill="none" stroke="#aaaaaa" d="M61.703125,143.4296875H521" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="49.203125" y="84.21484375" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4.28515625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">22,500</tspan></text><path fill="none" stroke="#aaaaaa" d="M61.703125,84.21484375H521" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="49.203125" y="25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4.2734375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">30,000</tspan></text><path fill="none" stroke="#aaaaaa" d="M61.703125,25H521" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="436.7304640036452" y="274.359375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,6.5703)"><tspan dy="4.2734375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013</tspan></text><text x="232.474502582017" y="274.359375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,6.5703)"><tspan dy="4.2734375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012</tspan></text><path fill="#74a5c1" stroke="none" d="M61.703125,219.76156875C74.53888213851762,220.2747640625,100.21039641555285,223.3430798828125,113.04615355407047,221.81435C125.8819106925881,220.28562011718748,151.55342496962334,209.80609741290982,164.38918210814094,207.5317296875C177.08542014732686,205.28208335040983,202.47789622569866,205.5371763671875,215.17413426488457,203.71829375C227.8703723040705,201.8994111328125,253.26284838244226,195.5316420615608,265.9590864216282,192.98066875C278.7948435601458,190.4016627646858,304.466357837181,183.09080292968753,317.30211497569866,183.19837656250002C330.1378721142163,183.3059501953125,355.8093863912515,204.83304284494537,368.64514352976914,193.8412578125C381.34138156895506,182.96894870432035,406.73385764732683,102.22413880352212,419.43009568651274,95.74200000000002C431.98681462636694,89.33109349102212,457.1002525060753,135.53908182091345,469.6569714459295,142.2690765625C482.49272858444715,149.14862674278848,508.16424286148236,148.20240390625003,521,150.18017968750002L521,261.859375L61.703125,261.859375Z" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path><path fill="none" stroke="#3c8dbc" d="M61.703125,219.76156875C74.53888213851762,220.2747640625,100.21039641555285,223.3430798828125,113.04615355407047,221.81435C125.8819106925881,220.28562011718748,151.55342496962334,209.80609741290982,164.38918210814094,207.5317296875C177.08542014732686,205.28208335040983,202.47789622569866,205.5371763671875,215.17413426488457,203.71829375C227.8703723040705,201.8994111328125,253.26284838244226,195.5316420615608,265.9590864216282,192.98066875C278.7948435601458,190.4016627646858,304.466357837181,183.09080292968753,317.30211497569866,183.19837656250002C330.1378721142163,183.3059501953125,355.8093863912515,204.83304284494537,368.64514352976914,193.8412578125C381.34138156895506,182.96894870432035,406.73385764732683,102.22413880352212,419.43009568651274,95.74200000000002C431.98681462636694,89.33109349102212,457.1002525060753,135.53908182091345,469.6569714459295,142.2690765625C482.49272858444715,149.14862674278848,508.16424286148236,148.20240390625003,521,150.18017968750002" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="61.703125" cy="219.76156875" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="113.04615355407047" cy="221.81435" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="164.38918210814094" cy="207.5317296875" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="215.17413426488457" cy="203.71829375" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="265.9590864216282" cy="192.98066875" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="317.30211497569866" cy="183.19837656250002" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="368.64514352976914" cy="193.8412578125" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="419.43009568651274" cy="95.74200000000002" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="469.6569714459295" cy="142.2690765625" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="521" cy="150.18017968750002" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><path fill="#eaf2f5" stroke="none" d="M61.703125,240.810471875C74.53888213851762,240.589403125,100.21039641555285,242.14280585937502,113.04615355407047,239.92619687500002C125.8819106925881,237.70958789062502,151.55342496962334,224.05899165812843,164.38918210814094,223.07760000000002C177.08542014732686,222.10687564250344,202.47789622569866,233.990895703125,215.17413426488457,232.1177328125C227.8703723040705,230.244569921875,253.26284838244226,209.9601315082821,265.9590864216282,208.092296875C278.7948435601458,206.20393658640708,304.466357837181,215.128994140625,317.30211497569866,217.092953125C330.1378721142163,219.05691210937502,355.8093863912515,233.13463180498633,368.64514352976914,223.80396875C381.34138156895506,214.57472594561133,406.73385764732683,148.6755446844786,419.43009568651274,142.8533296875C431.98681462636694,137.0950950751036,457.1002525060753,171.00009705958104,469.6569714459295,177.4821703125C482.49272858444715,184.10828963770604,508.16424286148236,190.835117578125,521,195.2861L521,261.859375L61.703125,261.859375Z" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path><path fill="none" stroke="#a0d0e0" d="M61.703125,240.810471875C74.53888213851762,240.589403125,100.21039641555285,242.14280585937502,113.04615355407047,239.92619687500002C125.8819106925881,237.70958789062502,151.55342496962334,224.05899165812843,164.38918210814094,223.07760000000002C177.08542014732686,222.10687564250344,202.47789622569866,233.990895703125,215.17413426488457,232.1177328125C227.8703723040705,230.244569921875,253.26284838244226,209.9601315082821,265.9590864216282,208.092296875C278.7948435601458,206.20393658640708,304.466357837181,215.128994140625,317.30211497569866,217.092953125C330.1378721142163,219.05691210937502,355.8093863912515,233.13463180498633,368.64514352976914,223.80396875C381.34138156895506,214.57472594561133,406.73385764732683,148.6755446844786,419.43009568651274,142.8533296875C431.98681462636694,137.0950950751036,457.1002525060753,171.00009705958104,469.6569714459295,177.4821703125C482.49272858444715,184.10828963770604,508.16424286148236,190.835117578125,521,195.2861" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="61.703125" cy="240.810471875" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="113.04615355407047" cy="239.92619687500002" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="164.38918210814094" cy="223.07760000000002" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="215.17413426488457" cy="232.1177328125" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="265.9590864216282" cy="208.092296875" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="317.30211497569866" cy="217.092953125" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="368.64514352976914" cy="223.80396875" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="419.43009568651274" cy="142.8533296875" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="469.6569714459295" cy="177.4821703125" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="521" cy="195.2861" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle></svg><div class="morris-hover morris-default-style" style="left: 322.145px; top: 128px; display: none;"><div class="morris-hover-row-label">2012 Q3</div><div class="morris-hover-point" style="color: #a0d0e0">
                                Item 1:
                                4,820
                            </div><div class="morris-hover-point" style="color: #3c8dbc">
                                Item 2:
                                3,795
                            </div></div></div>
                    <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"><svg height="300" version="1.1" width="577" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="none" stroke="#3c8dbc" d="M288.5,243.33333333333331A93.33333333333333,93.33333333333333,0,0,0,376.7277551949771,180.44625304313007" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#3c8dbc" stroke="#ffffff" d="M288.5,246.33333333333331A96.33333333333333,96.33333333333333,0,0,0,379.56364732624417,181.4248826052307L416.1151459070204,194.03833029452744A135,135,0,0,1,288.5,285Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#f56954" d="M376.7277551949771,180.44625304313007A93.33333333333333,93.33333333333333,0,0,0,204.78484627831412,108.73398312817662" stroke-width="2" opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#f56954" stroke="#ffffff" d="M379.56364732624417,181.4248826052307A96.33333333333333,96.33333333333333,0,0,0,202.09400205154566,107.40757544301087L162.92726941747117,88.10097469226493A140,140,0,0,1,420.8416327924656,195.6693795646951Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#00a65a" d="M204.78484627831412,108.73398312817662A93.33333333333333,93.33333333333333,0,0,0,288.47067846904883,243.333328727518" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#00a65a" stroke="#ffffff" d="M202.09400205154566,107.40757544301087A96.33333333333333,96.33333333333333,0,0,0,288.46973599126824,246.3333285794739L288.4575884998742,284.9999933380171A135,135,0,0,1,167.4120097954186,90.31165416754118Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="288.5" y="140" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#000000" font-size="15px" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: 800; font-stretch: normal; font-size: 15px; line-height: normal; font-family: Arial;" font-weight="800" transform="matrix(1,0,0,1,0,0)"><tspan dy="140" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">In-Store Sales</tspan></text><text x="288.5" y="160" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#000000" font-size="14px" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: Arial;" transform="matrix(1,0,0,1,0,0)"><tspan dy="160" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">30</tspan></text></svg></div>
                </div>
            </div>
            <!-- /.nav-tabs-custom -->



        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable ui-sortable">

            <!-- TO DO List -->
            <div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                    <i class="ion ion-clipboard"></i>

                    <h3 class="box-title">Perlu Perhatian</h3>

                    <div class="box-tools pull-right">
                        <ul class="pagination pagination-sm inline">
                            <li><a href="#">«</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">»</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="todo-list ui-sortable">
                        <li>
                            <!-- drag handle -->
                            <span class="handle ui-sortable-handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                            <!-- checkbox -->
                            <input type="checkbox" value="">
                            <!-- todo text -->
                            <span class="text">Aircond 5 menghampiri tarikh servis</span>
                            <!-- Emphasis label -->
                            <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 hari</small>
                            <!-- General tools such as edit or delete-->
                            <div class="tools">
                                <i class="fa fa-edit"></i>
                                <i class="fa fa-trash-o"></i>
                            </div>
                        </li>
                        <li>
                            <span class="handle ui-sortable-handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                            <input type="checkbox" value="">
                            <span class="text">Aircond 3 operasi lebih masa</span>
                            <small class="label label-info"><i class="fa fa-clock-o"></i> 12 jam</small>
                            <div class="tools">
                                <i class="fa fa-edit"></i>
                                <i class="fa fa-trash-o"></i>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix no-border">
                    <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                </div>
            </div>
            <!-- /.box -->
        </section>


    </div>
</div>
