<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Geolocation */

?>
<div class="geolocation-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
