<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Geolocation */
?>
<div class="geolocation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
