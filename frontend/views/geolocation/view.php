<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Geolocation */
?>
<div class="geolocation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'short_code',
            'description',
            'status',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'deleted',
            'deleted_at',
        ],
    ]) ?>

</div>
