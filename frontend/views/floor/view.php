<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Floor */
?>
<div class="floor-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'location_id',
            'type',
            'description',
            'status',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'deleted',
            'deleted_at',
        ],
    ]) ?>

</div>
