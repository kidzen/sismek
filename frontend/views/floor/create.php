<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Floor */

?>
<div class="floor-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
