<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Floor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="floor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
            $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(common\models\Sites::find()->asArray()->all(), 'id', ['site_id','description']), ['placeholder' => 'select'])
            ->label(Html::tag('span', 'Location')
                    . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/location/create']
                                    , ['role' => 'modal-remote', 'title' => 'Create new Floor', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
            )
    ?>                

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
