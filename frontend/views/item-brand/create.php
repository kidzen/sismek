<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ItemBrand */

?>
<div class="item-brand-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
