<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ItemType */
?>
<div class="item-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
