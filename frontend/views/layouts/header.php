<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

frontend\assets\AdminLteAsset::register($this);
$avatar = isset(Yii::$app->user->avatar) ? $directoryAsset . '/img/mpsp/tiada_gambar.jpg' : $directoryAsset . '/img/mpsp/tiada_gambar.jpg';
?>
<?php
if (isset(Yii::$app->user->avatar)) {
    echo Yii::getAlias('@web/') . Yii::$app->user->avatar;
} else {
    echo $directoryAsset . '/img/tiada_gambar.jpg';
}
?>
<header class="main-header">
    <?= Html::a('<span class="logo-mini">  <img src="' . $directoryAsset . '/img/mpsp/favicon-32x32.png"  alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/> </span><span class="logo-lg"><img src="' . $directoryAsset . '/img/mpsp/favicon-32x32.png" style="padding-right: 10px" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai">Sistem Mekanikal</></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?= Yii::$app->user->name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <?php if (isset(Yii::$app->user->id)) { ?>

                                <?=
                                Html::a(
                                        'Log Keluar', ['/site/logout'], ['data-method' => 'post']
                                )
                                ?>
                            <?php } else { ?>
                                <?=
                                Html::a(
                                        'Log Masuk', ['/site/login'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                )
                                ?>
                            <?php } ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
