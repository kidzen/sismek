<?php

use yii\helpers\Html;


//dmstr\web\AdminLteAsset::register($this);
frontend\assets\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist');
$avatar = isset(Yii::$app->user->avatar) ? $directoryAsset . '/img/mpsp/tiada_gambar.jpg' : $directoryAsset . '/img/mpsp/tiada_gambar.jpg';

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="<?php echo $directoryAsset; ?>/img/favicon.ico" type="image/x-icon" />
        <?php $this->head() ?>
    </head>
    <!--<body class="hold-transition skin-purple-light sidebar-mini">-->
    <body class="hold-transition skin-blue-light fixed sidebar-mini">
    <!--<body class="sidebar-mini skin-blue-light sidebar-collapse">-->
        <?php $this->beginBody() ?>
        <div class="wrapper">

            <?=
            $this->render(
                    'header.php', [
                'directoryAsset' => $directoryAsset,
                'username' => isset(Yii::$app->user->username) ? Yii::$app->user->username : 'Guest',
                'activeBool' => Yii::$app->user->status,
                'roles' => isset(Yii::$app->user->role) ? Yii::$app->user->role : 'Guest',
                    ]
            )
            ?>

            <?=
            $this->render(
                    'left.php', [
                'directoryAsset' => $directoryAsset,
                'username' => isset(Yii::$app->user->username) ? Yii::$app->user->username : 'Guest',
                'activeBool' => Yii::$app->user->status,
                'roles' => isset(Yii::$app->user->role) ? Yii::$app->user->role : 'Guest',
                    ]
            )
            ?>

            <?=
            $this->render(
                    'content.php', [
                'content' => $content,
                'directoryAsset' => $directoryAsset,
                'username' => isset(Yii::$app->user->username) ? Yii::$app->user->username : 'Guest',
                'activeBool' => Yii::$app->user->status,
                'roles' => isset(Yii::$app->user->role) ? Yii::$app->user->role : 'Guest',
                    ]
            )
            ?>

        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
