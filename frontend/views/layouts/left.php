<aside class="main-sidebar">

    <section class="sidebar">
        <!--            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 409px;">
                        <section class="sidebar" style="height: 409px; overflow: hidden; width: auto;">-->

        <!-- Sidebar user panel -->
        <!--        <div class="user-panel">
                                <div class="pull-left image">
                                    <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                                </div>
                                <div class="pull-left info">
                                    <p><?= $username ?></p>
        <?php if (Yii::$app->user->id) { ?>
                                                                                                                                                                                                                                    <a href="#"><i class="fa fa-circle text-success"></i> <?= $activeBool ?></a>
        <?php } else { ?>
                                                                                                                                                                                                                                    <a href="#"><i class="fa fa-circle text-danger"></i> <?= $activeBool ?></a>
        <?php } ?>
                                    
                                </div>
                    <div style="text-align: center">
                        <img src="<?= $directoryAsset ?>/img/MPSP.png" style="width: 200px" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/>
                        <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" style="width: 200px" alt="TUDM"/>
                    </div>
                </div>-->

        <!--search form--> 
        <!--                        <form action="#" method="get" class="sidebar-form">
                                    <div class="input-group">
                                        <input type="text" name="q" class="form-control" placeholder="Search..."/>
                                      <span class="input-group-btn">
                                        <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                                        </button>
                                      </span>
                                    </div>
                                </form>-->
        <!-- /.search form -->
        <div style="overflow: true">
            <?=
            dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu delay'],
                        'items' => [
                            ['label' => 'Menu', 'options' => ['class' => 'header']],
                            ['label' => 'Login', 'icon' => 'fa fa-sign-in', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                            [
                                'label' => 'Inventory',
                                'icon' => 'fa fa-download',
                                'style' => 'color:green;',
                                'visible' => !Yii::$app->user->isGuest,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Senarai Item', 'icon' => 'fa fa-caret-right', 'url' => ['item/index'],],
//                                    ['label' => 'Selenggara', 'icon' => 'fa fa-caret-right', 'url' => ['m_request/create'],],
                                ],
                            ],
                            [
                                'label' => 'Permohonan',
                                'icon' => 'fa fa-download',
                                'style' => 'color:green;',
                                'visible' => !Yii::$app->user->isGuest,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Permohonan Penyelenggaraan Kerosakan', 'icon' => 'fa fa-caret-right', 'url' => ['maintenance-request/create'],],
                                    ['label' => 'Selenggara', 'icon' => 'fa fa-caret-right', 'url' => ['maintenance-request/index'],],
                                ],
                            ],
                            [
                                'label' => 'Penyelengaraan',
                                'icon' => 'fa fa-upload',
                                'style' => 'color:red;',
                                'visible' => false,
//                                'visible' => !Yii::$app->user->isGuest,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Arahan Kerja', 'icon' => 'fa fa-caret-right', 'url' => ['laporan-penyelengaraan-sisken/index'],],
//                                    ['label' => 'Permohonan Penyelenggaraan', 'icon' => 'fa fa-caret-right', 'url' => ['request/create'],],
                                    ['label' => 'Sejarah Penyelenggaraan', 'icon' => 'fa fa-caret-right', 'url' => ['maintenance-history/index'],],
                                ],
                            ],
                            [
                                'label' => 'Pentadbiran Inventori',
                                'icon' => 'fa fa-archive',
                                'style' => 'color:#BF8233;',
                                'visible' => !Yii::$app->user->isGuest,
//                                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Senarai Kategori', 'icon' => 'fa fa-caret-right', 'url' => ['category/index'],],
//                                    ['label' => 'Kategori', 'icon' => 'fa fa-caret-right',
//                                        'items' => [
//                                            ['label' => 'Senarai Kategori', 'url' => ['category/index'],],
//                                            ['label' => 'Kelas Kategori', 'url' => ['category-class/index'],],
//                                            ['label' => 'Jenis Kategori', 'url' => ['category-type/index'],],
//                                        ],
//                                    ],
                                    ['label' => 'Setting', 'icon' => 'fa fa-caret-right',
                                        'items' => [
                                            ['label' => 'Kelas Kategori', 'icon' => 'fa fa-caret-right', 'url' => ['category-class/index'],],
                                            ['label' => 'Jenis Kategori', 'icon' => 'fa fa-caret-right', 'url' => ['category-type/index'],],
                                            ['label' => 'Daerah', 'icon' => 'fa fa-caret-right', 'url' => ['geolocation/index'],],
                                            ['label' => 'Lokasi', 'icon' => 'fa fa-caret-right', 'url' => ['location/index'],],
                                            ['label' => 'Site', 'icon' => 'fa fa-caret-right', 'url' => ['sites/index'],],
                                            ['label' => 'Floor', 'icon' => 'fa fa-caret-right', 'url' => ['floor/index'],],
                                            ['label' => 'Brand', 'icon' => 'fa fa-caret-right', 'url' => ['item-brand/index'],],
                                            ['label' => 'Model', 'icon' => 'fa fa-caret-right', 'url' => ['item-model/index'],],
                                            ['label' => 'Item Type', 'icon' => 'fa fa-caret-right', 'url' => ['item-type/index'],],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                'label' => 'Pentadbiran Sistem',
                                'icon' => 'fa fa-gear',
                                'style' => 'color:blue;',
                                'visible' => false,
//                                'visible' => !Yii::$app->user->isGuest,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Akses Sistem', 'icon' => 'fa fa-caret-right', 'url' => ['roles/index'],
                                        'visible' => !Yii::$app->user->isGuest,
//                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,
                                    ],
                                    ['label' => 'Pengguna', 'icon' => 'fa fa-caret-right', 'url' => ['people/index'],
                                        'visible' => !Yii::$app->user->isGuest,
//                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,
                                    ],
                                    ['label' => 'Pofil Saya', 'icon' => 'fa fa-user', 'url' => ['user/view', 'id' => Yii::$app->user->id],],
                                    ['label' => 'Aktiviti Log', 'icon' => 'fa fa-caret-right', 'url' => ['activity-logs/index'],
                                        'visible' => Yii::$app->user->isAdmin
                                    ],
                                    ['label' => 'Penyegerakan Sistem', 'icon' => 'fa fa-refresh', 'url' => ['site/maintenance']],
                                ],
                            ],
                            [
                                'label' => 'Dalam Pembangunan',
                                'icon' => 'fa fa-caret-right',
                                'style' => 'color:blue;',
                                'visible' => false,
//                                'visible' => !Yii::$app->user->isGuest,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Borang', 'icon' => 'fa fa-caret-right', 'url' => ['official-form/index'],
                                        'visible' => Yii::$app->user->isAdmin],
                                    ['label' => 'Laporan Kenderaan', 'icon' => 'fa fa-truck', 'url' => ['vehicle-report/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,],
                                    ['label' => 'Laporan Vendor', 'icon' => 'fa fa-building', 'url' => ['vendors-report/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,],
                                    ['label' => 'Laporan Suku Tahunan', 'icon' => 'fa fa-building', 'url' => ['report/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,],
//                                    ['label' => 'Completed Work Order', 'icon' => 'fa fa-building', 'url' => ['vendors-report/index'],
//                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,],
                                    ['label' => 'Senarai Transaksi', 'icon' => 'fa fa-building', 'url' => ['transactions/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,],
                                ],
                            ],
                            [
                                'label' => 'Developer Tools',
                                'icon' => 'fa fa-share',
//                                'visible' => Yii::$app->user->isAdmin,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Gii', 'icon' => 'fa fa-caret-right', 'url' => ['/gii'],],
                                    ['label' => 'Debug', 'icon' => 'fa fa-caret-right', 'url' => ['/debug'],],
                                    [
                                        'label' => 'Pengkalan Data',
                                        'icon' => 'fa fa-share',
//                                        'visible' => Yii::$app->user->isAdmin, //&& false,
                                        'url' => '#',
                                        'items' => [
//                                    ['label' => 'Home', 'icon' => 'fa fa-home', 'url' => ['site/index']],
//                                    ['label' => 'User', 'icon' => 'fa fa-user', 'url' => ['people/index']],
//                                    ['label' => 'Roles', 'icon' => 'fa fa-users', 'url' => ['/estor-roles/index']],
//                                    ['label' => 'Inventories', 'icon' => 'fa fa-archive', 'url' => ['/estor-inventories/index']],
                                            ['label' => 'Orders', 'icon' => 'fa fa-cart-plus', 'url' => ['orders/index']],
                                            ['label' => 'Order Items', 'icon' => 'fa fa-cubes', 'url' => ['order-items/index']],
                                            ['label' => 'Transactions', 'icon' => 'fa fa-building', 'url' => ['transactions/index']],
//                                    ['label' => 'Arahan Kerja Sisken', 'icon' => 'fa fa-building', 'url' => ['laporan-penyelengaraan-sisken/index']],
//                                    ['label' => 'Inventory Items', 'icon' => 'fa fa-cubes', 'url' => ['inventory-items/index']],
//                                    ['label' => 'Vendors', 'icon' => 'fa fa-building', 'url' => ['/vendors/index']],
//                                    ['label' => 'Categories', 'icon' => 'fa fa-tags', 'url' => ['/categories/index']],
//                                    ['label' => 'Usage List', 'icon' => 'fa fa-tags', 'url' => ['/usage-list/index']],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ]
            )
            ?>
            <!--        </div>
                </section>-->


            <!--                <div class="slimScrollBar" style="width: 3px; position: absolute; top: 31px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 169.656px; background: rgba(0, 0, 0, 0.2);">
                            </div>
                            <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);">
                            </div>
                            </div>-->
            </aside>
