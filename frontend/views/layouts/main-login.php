<?php

use backend\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

//dmstr\web\AdminLteAsset::register($this);
frontend\assets\AdminLteAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist');
$avatar = isset(Yii::$app->user->avatar) ? $directoryAsset . '/img/mpsp/tiada_gambar.jpg' : $directoryAsset . '/img/mpsp/tiada_gambar.jpg';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!--<link rel="stylesheet" href="owl-carousel/owl.carousel.css">-->

        <!-- Default Theme -->
        <!--<link rel="stylesheet" href="owl-carousel/owl.theme.css">-->

        <!-- Include js plugin -->
        <!--<script src="owl-carousel/owl.carousel.js"></script>-->
    </head>
    <body class="skin-blue-light layout-top-nav" data-gr-c-s-loaded="true">
        <?php $this->beginBody() ?>
        <div class="wrapper">

            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="../../index2.html" class="navbar-brand"><b>SISMECH</b>Sistem Mekanikal MPSP</a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->

                        <!-- /.navbar-collapse -->
                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <div class="nav navbar-nav" style="padding-top: 8px">
                                <!-- Messages: style can be found in dropdown.less-->
                                <?= $content ?>
                                <!-- /.messages-menu -->

                                <!-- Notifications Menu -->

                            </div>
                        </div>
                        <!-- /.navbar-custom-menu -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </header>
            <!-- Full Width Column -->
            <?php
            echo yii\bootstrap\Carousel::widget([
                'items' => [
                    // the item contains only the image
                    Html::img($directoryAsset . '/img/mpsp/MPSP1.jpg',['style'=>'height:650px;width:100%']),
                    Html::img($directoryAsset . '/img/mpsp/MPSP2.jpg',['style'=>'height:650px;width:100%']),
                    Html::img($directoryAsset . '/img/mpsp/MPSP3.jpg',['style'=>'height:650px;width:100%']),
                ]
            ]);
            ?>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

