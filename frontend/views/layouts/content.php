<?php

use yii\widgets\Breadcrumbs;
//use dmstr\widgets\Alert;
//use Yii;
use kartik\widgets\Growl;
use yii\helpers\Html;
use yii\bootstrap\Modal;

?>
<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header']; ?></h1>
        <?php } else { ?>
            <h1>
                <?php
                if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                            \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                }
                ?>
            </h1>

        <?php } ?>

    </section>
    <section class="content">
        <?php
        $delay = 0;
        foreach (Yii::$app->session->getAllFlashes() as $message):;
            echo \kartik\widgets\Growl::widget([
                'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
                'title' => (!empty($message['title'])) ? Html::encode($message['title']) : null,
                'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
                'body' => (!empty($message['message'])) ? $message['message'] : 'Message Not Set!',
                'showSeparator' => true,
                'delay' => $delay, //This delay is how long before the message shows
                'pluginOptions' => [
                    'showProgressbar' => true,
                    'delay' => (!empty($message['duration'])) ? $message['duration'] + $delay : 3000, //This delay is how long the message shows for
                    'placement' => [
                        'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                        'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
                    ]
                ]
            ]);
            $delay = $delay + 1000;
            ?>
        <?php endforeach; ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; <?=
        date('Y');
        echo Html::a(' Majlis Perbandaran Seberang Perai', false)
        ?>.</strong> All rights
    reserved.
</footer>
