<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\CategoryClass;
use common\models\CategoryType;
/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">
    <?php $form = ActiveForm::begin(); ?>
    <?=
            $form->field($model, 'class_id')->dropDownList(ArrayHelper::map(CategoryClass::find()->asArray()->all(), 'id', 'description'), ['placeholder' => 'select'])
            ->label(Html::tag('span', 'Class')
                    . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/category-class/create']
                                    , ['role' => 'modal-remote', 'title' => 'Create new Categories', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
            )
    ?>                

    <?=
            $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(CategoryType::find()->asArray()->all(), 'id', 'description'))
            ->label(Html::tag('span', 'Type')
                    . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/category-type/create']
                                    , ['role' => 'modal-remote', 'title' => 'Create new Categories', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
            )
    ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>
    <?= Html::a('Back', [\Yii::$app->request->referrer],['class' =>  'btn btn-primary']) ?>
    <?php ActiveForm::end(); ?>
</div>
