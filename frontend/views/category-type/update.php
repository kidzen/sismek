<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryType */
?>
<div class="category-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
