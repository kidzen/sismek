<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Item */

?>
<div class="item-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
