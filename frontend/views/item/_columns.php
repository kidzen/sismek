<?php

use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'category.description',
        'label' => 'Category',
//        'group' => true,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'location.name',
        'format' => 'raw',
        'label' => 'Location',
//        'group' => true,
//        'subGroupOf' => 1,
        'value' => function ($model) {
            $return = '<div class="row">'
                    . '<div class="col-sm-6">'
                    . 'Lokasi: '
                    . '</div>'
                    . '<div class="col-sm-6">'
                    . isset($model->location) ? $model->location->name : null
                    . '</div>'
                    . '<div class="col-sm-6">'
                    . 'Tingkat: '
                    . '</div>'
                    . '<div class="col-sm-6">'
                    . isset($model->location) ? $model->location->floor->name : null
                            . '</div>'
                            . '</div>';
            $location = isset($model->location) ? $model->location->name : null;
            $site = isset($model->location) ? $model->location->site->name : null;
            $floor = isset($model->location) ? $model->location->floor->name : null;
            $return2 = '<span style="color:green;">Bahagian</span> :<br>'
                    . $site
                    . '<br><br><span style="color:green;">Lokasi</span>: <br>'
                    . $location . ' <br>(Aras ' . $floor . ')';

            return $return2;
        },
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'type',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'detail',
        'format' => 'raw',
        'label' => 'Details',
        'value' => function ($model) {
            $brand = isset($model->brand) ? $model->itemBrand->name : null;
            $model = isset($model->model) ? $model->itemModel->name : null;
            $return2 = '<span style="color:green;">Brand</span> :<br>'
                    . $brand
                    . '<br><br><span style="color:green;">Model</span>: <br>'
                    . $model
            ;
            return $return2;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'horse_power',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'serial_no',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'kew_pa',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'start_service',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'end_of_service',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'status',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'deleted',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'deleted_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
                'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
                'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
                    'data-confirm' => false, 'data-method' => false, // for overide yii data api
                    'data-request-method' => 'post',
                    'data-toggle' => 'tooltip',
                    'data-confirm-title' => 'Are you sure?',
                    'data-confirm-message' => 'Are you sure want to delete this item'],
            ],
        ];
        