<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DateTimePicker;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">
    <?php
    Pjax::begin([
        'timeout' => false,
    ])
    ?>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-3">

            <?=
                    $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(common\models\Category::find()->asArray()->all(), 'id', 'description'), ['placeholder' => 'select'])
                    ->label(Html::tag('span', 'Category')
                            . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/category/create']
                                            , ['role' => 'modal-remote', 'title' => 'Create new Categories', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
                    )
            ?>                
        </div>
        <div class="col-md-3">
            <?=
                    $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(common\models\Location::find()->asArray()->all(), 'id', 'name'), ['placeholder' => 'select'])
                    ->label(Html::tag('span', 'Location')
                            . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/location/create']
                                            , ['role' => 'modal-remote', 'title' => 'Create new Floor', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
                    )
            ?>                
        </div>
        <div class="col-md-3">

            <?=
                    $form->field($model, 'type')->dropDownList(ArrayHelper::map(common\models\ItemType::find()->asArray()->all(), 'id', 'name'), ['placeholder' => 'select'])
                    ->label(Html::tag('span', 'Type')
                            . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/item-type/create']
                                            , ['role' => 'modal-remote', 'title' => 'Create new Floor', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
                    )
            ?>                
        </div>
        <div class="col-md-3">
            <?=
                    $form->field($model, 'brand')->dropDownList(ArrayHelper::map(common\models\ItemBrand::find()->asArray()->all(), 'id', 'name'), ['placeholder' => 'select'])
                    ->label(Html::tag('span', 'Brand')
                            . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/item-brand/create']
                                            , ['role' => 'modal-remote', 'title' => 'Create new Floor', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
                    )
            ?>                
        </div>
        <div class="col-md-3">
            <?=
                    $form->field($model, 'model')->dropDownList(ArrayHelper::map(common\models\ItemModel::find()->asArray()->all(), 'id', 'name'), ['placeholder' => 'select'])
                    ->label(Html::tag('span', 'Model')
                            . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/item-model/create']
                                            , ['role' => 'modal-remote', 'title' => 'Create new Floor', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
                    )
            ?>                
        </div>
        <div class="col-md-4">

            <?= $form->field($model, 'horse_power')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">

            <?= $form->field($model, 'serial_no')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">

            <?= $form->field($model, 'kew_pa')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">

            <?=
            $form->field($model, 'start_service')->widget(DateTimePicker::className(), [
                'type' => 3,
                'pluginOptions' => [
                    'format' => 'dd-M-yyyy H:ii:ss',
                    'autoclose' => true,
                    'showToday' => true,
                ]
            ])
            ?>
        </div>
        <div class="col-md-6">

            <?=
            $form->field($model, 'end_of_service')->widget(DateTimePicker::className(), [
                'type' => 3,
                'pluginOptions' => [
                    'autoclose' => true,
                ]
            ])
            ?>

        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>
</div>
</div>
