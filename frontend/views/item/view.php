<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Item */
?>
<div class="item-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_id',
            'location_id',
            'type',
            'brand',
            'model',
            'horse_power',
            'serial_no',
            'kew_pa',
            'start_service',
            'end_of_service',
            'status',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'deleted',
            'deleted_at',
        ],
    ]) ?>

</div>
