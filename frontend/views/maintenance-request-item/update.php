<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MaintenanceRequestItem */
?>
<div class="maintenance-request-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
