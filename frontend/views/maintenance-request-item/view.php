<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MaintenanceRequestItem */
?>
<div class="maintenance-request-item-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'transaction_id',
            'items_id',
            'maintenance_type',
            'status',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'deleted',
            'deleted_at',
        ],
    ]) ?>

</div>
