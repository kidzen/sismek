<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MaintenanceRequest;
use common\models\Item;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\MaintenanceRequestItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maintenance-request-item-form">

    <?php $form = ActiveForm::begin(); ?>


    <?=
            $form->field($model, 'transaction_id')->dropDownList(ArrayHelper::map(MaintenanceRequest::find()->asArray()->all(), 'id', 'description'))
            ->label(Html::tag('span', 'Type')
                    . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/category-type/create']
                                    , ['role' => 'modal-remote', 'title' => 'Create new Categories', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
            )
    ?>
    <?=
            $form->field($model, 'items_id')->dropDownList(ArrayHelper::map(Item::find()->asArray()->all(), 'id', 'description'))
            ->label(Html::tag('span', 'Type')
                    . Html::tag('span', Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/category-type/create']
                                    , ['role' => 'modal-remote', 'title' => 'Create new Categories', 'class' => 'btn-xs btn-info']), ['style' => 'padding-left:10px'])
            )
    ?>

    <?= $form->field($model, 'maintenance_type')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
