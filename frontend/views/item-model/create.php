<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ItemModel */

?>
<div class="item-model-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
