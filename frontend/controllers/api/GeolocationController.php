<?php

namespace frontend\controllers\api;

/**
* This is the class for REST controller "GeolocationController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class GeolocationController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Geolocation';
}
