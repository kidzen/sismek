<?php

namespace frontend\controllers\api;

/**
* This is the class for REST controller "SitesController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class SitesController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Sites';
}
