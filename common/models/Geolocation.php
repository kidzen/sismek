<?php

namespace common\models;

use Yii;
use \common\models\base\Geolocation as BaseGeolocation;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "geolocation".
 */
class Geolocation extends BaseGeolocation
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
