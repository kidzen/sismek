<?php

namespace common\models;

use Yii;
use \common\models\base\Floor as BaseFloor;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "floor".
 */
class Floor extends BaseFloor
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
