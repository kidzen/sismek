<?php

namespace common\models;

use Yii;
use \common\models\base\ItemBrand as BaseItemBrand;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "item_brand_lookup".
 */
class ItemBrand extends BaseItemBrand
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
