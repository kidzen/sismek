<?php

namespace common\models;

use Yii;
use \common\models\base\MaintenanceRequest as BaseMaintenanceRequest;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "m_request".
 */
class MaintenanceRequest extends BaseMaintenanceRequest
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
