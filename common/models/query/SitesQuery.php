<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Sites]].
 *
 * @see \common\models\Sites
 */
class SitesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Sites[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Sites|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
