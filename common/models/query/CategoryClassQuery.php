<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\CategoryClass]].
 *
 * @see \common\models\CategoryClass
 */
class CategoryClassQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\CategoryClass[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\CategoryClass|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
