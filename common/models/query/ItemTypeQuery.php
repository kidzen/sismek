<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\ItemType]].
 *
 * @see \common\models\ItemType
 */
class ItemTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ItemType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ItemType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
