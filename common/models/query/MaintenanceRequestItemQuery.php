<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\MaintenanceRequestItem]].
 *
 * @see \common\models\MaintenanceRequestItem
 */
class MaintenanceRequestItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\MaintenanceRequestItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\MaintenanceRequestItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
