<?php

namespace common\models;

use Yii;
use \common\models\base\ItemModel as BaseItemModel;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "item_model_lookup".
 */
class ItemModel extends BaseItemModel
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
