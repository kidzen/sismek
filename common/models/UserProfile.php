<?php

namespace common\models;

use Yii;
use \common\models\base\UserProfile as BaseUserProfile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 */
class UserProfile extends BaseUserProfile
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
