<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace common\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base-model class for table "item".
 *
 * @property string $id
 * @property string $category_id
 * @property string $location_id
 * @property string $type
 * @property string $brand
 * @property string $model
 * @property string $horse_power
 * @property string $serial_no
 * @property string $kew_pa
 * @property string $start_service
 * @property string $end_of_service
 * @property integer $status
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\ItemBrand $brand0
 * @property \common\models\Category $category
 * @property \common\models\Location $location
 * @property \common\models\ItemModel $model0
 * @property string $aliasModel
 */
abstract class Item extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(), 'value' => new \yii\db\Expression('CURRENT_TIMESTAMP')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'location_id', 'type', 'brand', 'model'], 'number'],
            [['start_service', 'end_of_service', 'deleted_at'], 'safe'],
            [['status', 'deleted'], 'integer'],
            [['horse_power', 'serial_no', 'kew_pa'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['brand'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\ItemBrand::className(), 'targetAttribute' => ['brand' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\Location::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['model'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\ItemModel::className(), 'targetAttribute' => ['model' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'location_id' => Yii::t('app', 'Location ID'),
            'type' => Yii::t('app', 'Type'),
            'brand' => Yii::t('app', 'Brand'),
            'model' => Yii::t('app', 'Model'),
            'horse_power' => Yii::t('app', 'Horse Power'),
            'serial_no' => Yii::t('app', 'Serial No'),
            'kew_pa' => Yii::t('app', 'Kew Pa'),
            'start_service' => Yii::t('app', 'Start Service'),
            'end_of_service' => Yii::t('app', 'End Of Service'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted' => Yii::t('app', 'Deleted'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBrand()
    {
        return $this->hasOne(\common\models\ItemBrand::className(), ['id' => 'brand']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(\common\models\Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(\common\models\Location::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemModel()
    {
        return $this->hasOne(\common\models\ItemModel::className(), ['id' => 'model']);
    }


    
    /**
     * @inheritdoc
     * @return \common\models\query\ItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ItemQuery(get_called_class());
    }


}
