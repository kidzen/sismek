<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace common\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base-model class for table "m_request_item".
 *
 * @property string $id
 * @property string $transaction_id
 * @property string $items_id
 * @property string $maintenance_type
 * @property integer $status
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\MRequest $transaction
 * @property string $aliasModel
 */
abstract class MaintenanceRequestItem extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_request_item';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(), 'value' => new \yii\db\Expression('CURRENT_TIMESTAMP')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'transaction_id', 'items_id'], 'number'],
            [['status', 'deleted'], 'integer'],
            [['deleted_at'], 'safe'],
            [['maintenance_type'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['transaction_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\MaintenanceRequest::className(), 'targetAttribute' => ['transaction_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'items_id' => Yii::t('app', 'Items ID'),
            'maintenance_type' => Yii::t('app', 'Maintenance Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted' => Yii::t('app', 'Deleted'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(\common\models\MRequest::className(), ['id' => 'transaction_id']);
    }


    
    /**
     * @inheritdoc
     * @return \common\models\query\MaintenanceRequestItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MaintenanceRequestItemQuery(get_called_class());
    }


}
