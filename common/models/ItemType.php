<?php

namespace common\models;

use Yii;
use \common\models\base\ItemType as BaseItemType;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "item_type_lookup".
 */
class ItemType extends BaseItemType
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
