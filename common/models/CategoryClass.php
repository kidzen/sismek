<?php

namespace common\models;

use Yii;
use \common\models\base\CategoryClass as BaseCategoryClass;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category_class_lookup".
 */
class CategoryClass extends BaseCategoryClass
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
